import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EliminarpublicacionacionComponent } from './eliminarpublicacionacion.component';

describe('EliminarpublicacionacionComponent', () => {
  let component: EliminarpublicacionacionComponent;
  let fixture: ComponentFixture<EliminarpublicacionacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EliminarpublicacionacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EliminarpublicacionacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
