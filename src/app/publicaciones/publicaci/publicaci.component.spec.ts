import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicaciComponent } from './publicaci.component';

describe('PublicaciComponent', () => {
  let component: PublicaciComponent;
  let fixture: ComponentFixture<PublicaciComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicaciComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicaciComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
