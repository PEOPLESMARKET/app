import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PublicaciComponent } from './publicaci/publicaci.component';
import { VerpublicacionComponent} from './verpublicacion/verpublicacion.component';
import { CrearpublicacionComponent} from './crearpublicacion/crearpublicacion.component';
import { EliminarpublicacionacionComponent} from './eliminarpublicacionacion/eliminarpublicacionacion.component';
import { PublicacionesComponent } from './publicaciones.component';

const routes: Routes = [
{ path: '', component: PublicacionesComponent},
{ path: 'publicaci', component: PublicaciComponent},
{ path: 'verpublicacion', component: VerpublicacionComponent},
{ path: 'crearpublicacion', component: CrearpublicacionComponent},
{ path: 'eliminarpublicacionacion', component: EliminarpublicacionacionComponent},
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicacionesRoutingModule { }
