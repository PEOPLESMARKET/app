
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { PublicacionesRoutingModule } from './publicaciones-routing.module';
import { PublicacionesComponent } from './publicaciones.component';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import {PublicaciComponent} from '../publicaciones/publicaci/publicaci.component';
import {VerpublicacionComponent} from '../publicaciones/verpublicacion/verpublicacion.component';
import {CrearpublicacionComponent } from '../publicaciones/crearpublicacion/crearpublicacion.component';
import {MatTableModule} from '@angular/material/table';
import {MatMenuModule} from '@angular/material/menu';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatSortModule} from '@angular/material/sort';
import {MatTabsModule} from '@angular/material/tabs';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {EliminarpublicacionacionComponent} from '../publicaciones/eliminarpublicacionacion/eliminarpublicacionacion.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';


@NgModule({
  declarations: [PublicacionesComponent, PublicaciComponent, VerpublicacionComponent, CrearpublicacionComponent, EliminarpublicacionacionComponent],
  imports: [
    CommonModule,
    PublicacionesRoutingModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatIconModule,
    MatCardModule,
    MatSelectModule,
    MatTableModule,
    MatMenuModule,
    MatSidenavModule,
    MatListModule,
    MatSnackBarModule,
    MatSortModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatSlideToggleModule,

  ],
})
export class PublicacionesModule { }
